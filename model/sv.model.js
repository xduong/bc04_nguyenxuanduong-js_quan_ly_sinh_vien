// console.log("dương handsome")

// tạo khuôn Object

function SinhVien(msSV, ten, email, matKhau, diemToan, diemLy, diemHoa) {
  this.msSV = msSV;
  this.ten = ten;
  this.email = email;
  this.matKhau = matKhau;
  this.diemToan = diemToan;
  this.diemLy = diemLy;
  this.diemHoa = diemHoa;
  this.tinhDTB = function () {
    return (this.diemToan * 1 + this.diemLy * 1 + this.diemHoa * 1) / 3;
  };
}
