// console.log("duong handsome");

var validator = {
  kiemTraRong: function (value, idError, message) {
    if (value.length == 0) {
      document.getElementById(idError).innerHTML = message;
      return false;
    } else {
      document.getElementById(idError).innerHTML = "";
      return true;
    }
  },
  kiemTraChu: function (value, idError, message) {
    // var reg = /^[A-Za-z\s]+$/;
    // var reg = /[^a-z0-9A-Z_\x{00C0}-\x{00FF}\x{1EA0}-\x{1EFF}]/u;
    var reg =
      /[^a-z0-9A-Z_ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễếệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ]/u;

    if (reg.test(value)) {
      document.getElementById(idError).innerHTML = "";
      console.log("true");
      return true;
    } else {
      document.getElementById(idError).innerHTML = message;
      console.log("fla");
      return false;
    }
  },
  kiemTraSo: function (value, idError, message) {
    var reg = /\d+/g;
    if (reg.test(value)) {
      document.getElementById(idError).innerHTML = "";
      return true;
    } else {
      document.getElementById(idError).innerHTML = message;
      return false;
    }
  },
  kiemTraEmail: function (value, idError, message) {
    var reg = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6}$/;
    if (reg.test(value)) {
      document.getElementById(idError).innerHTML = "";
      return true;
    } else {
      document.getElementById(idError).innerHTML = message;
      return false;
    }
  }, // regex email js
  kiemTraChieuDai: function (value, idError, message, min, max) {
    if (value.length < min || value.length > max) {
      document.getElementById(idError).innerHTML = message;
      return false;
    } else {
      document.getElementById(idError).innerHTML = "";
      return true;
    }
  },
  kiemTraKhoangDiem: function (value, idError, message, min, max) {
    if (value * 1 < min || value * 1 > max) {
      document.getElementById(idError).innerHTML = message;
      return false;
    } else {
      document.getElementById(idError).innerHTML = "";
      return true;
    }
  },
  kiemTraTrungTen: function (vale, idError, message, dssv) {
    // console.log("vale: ", vale);
    if (timTenSV(vale, dssv) == -1 || updateSinhVien == true) {
      document.getElementById(idError).innerHTML = "";
      return true;
    } else {
      document.getElementById(idError).innerHTML = message;
      return false;
    }
  },
  kiemTraTrungMa: function (vale, idError, message, dssv) {
    console.log("timSV(vale, dssv): ", timSV(vale, dssv));
    if (timSV(vale, dssv) == -1 || updateSinhVien == true) {
      document.getElementById(idError).innerHTML = "";
      return true;
    } else {
      document.getElementById(idError).innerHTML = message;
      return false;
    }
  },
};
