// tạo key cho data dssv dưới local storage
const DSSV_LOCALSTORAGE = "DSSV";

// danh sách sinh viên ~ array của các object
var dssv = [];

// kiểm tra dữ liệu ở local storage
var dssvJson = localStorage.getItem(DSSV_LOCALSTORAGE);
if (dssvJson != null) {
  dssv = JSON.parse(dssvJson);
  for (var index = 0; index < dssv.length; index++) {
    var svIn4 = dssv[index];
    // console.log("svIn4: ", svIn4);
    dssv[index] = new SinhVien(
      svIn4.msSV,
      svIn4.ten,
      svIn4.email,
      svIn4.matKhau,
      svIn4.diemToan,
      svIn4.diemLy,
      svIn4.diemHoa
    );
  }
  renderDSSV(dssv);
}

// hàm thêm sinh viên
var themSinhVien = false;
var updateSinhVien = false;
function themSV() {
  var sv = getThongTinTuForm();

  // kiểm tra ô mã số sinh viên
  var isValid =
    validator.kiemTraRong(sv.msSV, "spanMaSV", "Ô này không được để trống") &&
    validator.kiemTraSo(sv.msSV, "spanMaSV", "Định dạng mã số không hợp lệ") &&
    validator.kiemTraChieuDai(
      sv.msSV,
      "spanMaSV",
      "mã số sinh viên có 4 số",
      4,
      4
    ) &&
    validator.kiemTraTrungMa(
      sv.msSV,
      "spanMaSV",
      "Trùng mã số sinh viên",
      dssv
    );

  // kiểm tra ô tên sinh viên
  isValid &=
    validator.kiemTraRong(sv.ten, "spanTenSV", "Ô này không được để trống") &&
    validator.kiemTraTrungTen(sv.ten, "spanTenSV", "Trùng tên", dssv);
  //  &&
  // validator.kiemTraChu(sv.ten, "spanTenSV", "tên không hợp lệ");

  // kiểm tra ô email
  isValid &=
    validator.kiemTraRong(
      sv.email,
      "spanEmailSV",
      "Ô này không được để trống"
    ) &&
    validator.kiemTraEmail(
      sv.email,
      "spanEmailSV",
      "Định dạng email không hợp lệ"
    );

  // kiểm tra ô mật khẩu

  // kiểm tra điểm toán
  isValid &=
    validator.kiemTraSo(sv.diemToan, "spanToan", "Định dạng không hợp lệ") &&
    validator.kiemTraKhoangDiem(
      sv.diemToan,
      "spanToan",
      "Định dạng không hợp lệ",
      0,
      10
    );

  // kiểm tra điểm lý
  isValid &=
    validator.kiemTraSo(sv.diemLy, "spanLy", "Định dạng không hợp lệ") &&
    validator.kiemTraKhoangDiem(
      sv.diemLy,
      "spanLy",
      "Định dạng không hợp lệ",
      0,
      10
    );

  // kiểm tra điển hoá
  isValid &=
    validator.kiemTraSo(sv.diemHoa, "spanHoa", "Định dạng không hợp lệ") &&
    validator.kiemTraKhoangDiem(
      sv.diemHoa,
      "spanHoa",
      "Định dạng không hợp lệ",
      0,
      10
    );

  // console.log("isValid: ", isValid);
  if (isValid) {
    dssv.push(sv);
    // console.log('dssv: ', dssv);
    // lưu data xuống local storage
    var dssvJson = JSON.stringify(dssv);
    localStorage.setItem(DSSV_LOCALSTORAGE, dssvJson);
    // in data ra table
    renderDSSV(dssv);
    delet();
    // console.log("true");
    themSinhVien = true;
    updateSinhVien = false;
  }
}

// hàm xoá sinh viên
function xoaSinhVien(msSV) {
  //   console.log("maSv: ", msSV);
  var index = timSV(msSV, dssv);
  if (index != -1) {
    dssv.splice(index, 1);
    renderDSSV(dssv);
  }
  localStorage.setItem(DSSV_LOCALSTORAGE, JSON.stringify(dssv));
}

// hàm sửa thông tin sinh viên
function suaSinhVien(msSV) {
  var index = timSV(msSV, dssv);
  if (index != -1) {
    showThongTinlenForm(dssv[index]);
  }
  document.getElementById("updateSinhVien").removeAttribute("disabled");
  document.getElementById("txtMaSV").setAttribute("disabled", "");
  document.getElementById("themSv").setAttribute("disabled", "");
}

// hàm cập nhật sau khi sửa sinh viên
function updateSv() {
  updateSinhVien = true;
  themSinhVien = false;
  themSV();
  if (themSinhVien) {
    var msSV = dssv[dssv.length - 1].msSV;
    xoaSinhVien(msSV);

    document.getElementById("updateSinhVien").setAttribute("disabled", "");
    document.getElementById("txtMaSV").removeAttribute("disabled");
    document.getElementById("themSv").removeAttribute("disabled");
  }
}

// hàm reset thông tin sinh viên
function resetSv() {
  dssv = [];
  localStorage.setItem(DSSV_LOCALSTORAGE, []);
  renderDSSV(dssv);
}

// hàm tìm kiếm sinh viên theo mã số sinh viên
document.getElementById("btnSearchFake").onclick = function () {
  document.getElementById("txtSearch").value = "";
  var maSv = document.getElementById("txtSearchFake").value;

  var contentHTML = "";
  var index = timSV(maSv, dssv);
  if (index != -1) {
    sv = dssv[index];
    contentHTML = `
    <p>Mã số sinh viên: ${sv.msSV}</p>
    <p> Tên: ${sv.ten}</p> 
    <p> Email: ${sv.email}</p>
    <p> Mật khẩu: ${sv.matKhau}</p>
    <p> Điểm toán: ${sv.diemToan}</p>
    <p> Điểm lý: ${sv.diemLy}</p>
    <p> Điểm hoá: ${sv.diemHoa}</p>
    <p> Điểm trung bình: ${sv.tinhDTB()}</p>
   `;
    document.getElementById("txtSearchFake").value = "";
  } else {
    contentHTML = "khum tìm thấy sinh viên";
  }
  document.getElementById("ketQuaSearch").innerHTML = contentHTML;
};

// hàm tìm kiếm sinh viên theo tên sinh viên
document.getElementById("btnSearch").onclick = function () {
  document.getElementById("txtSearchFake").value = "";
  var tenSv = document.getElementById("txtSearch").value;

  // console.log("tenSv: ", tenSv);

  var contentHTML = "";
  var index = timTenSV(tenSv, dssv);
  if (index != -1) {
    sv = dssv[index];
    contentHTML = `
    <p>Mã số sinh viên: ${sv.msSV}</p>
    <p> Tên: ${sv.ten}</p> 
    <p> Email: ${sv.email}</p>
    <p> Mật khẩu: ${sv.matKhau}</p>
    <p> Điểm toán: ${sv.diemToan}</p>
    <p> Điểm lý: ${sv.diemLy}</p>
    <p> Điểm hoá: ${sv.diemHoa}</p>
    <p> Điểm trung bình: ${sv.tinhDTB()}</p>
   `;
    document.getElementById("txtSearch").value = "";
  } else {
    contentHTML = "khum tìm thấy sinh viên";
  }
  document.getElementById("ketQuaSearch").innerHTML = contentHTML;
};
