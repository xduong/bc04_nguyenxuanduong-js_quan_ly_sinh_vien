// console.log("dương cute lạc lối");

// hàm lấy thông tin sinh viên từ form

function getThongTinTuForm() {
  const msSV = document.getElementById("txtMaSV").value;
  const ten = document.getElementById("txtTenSV").value;
  const email = document.getElementById("txtEmail").value;
  const matKhau = document.getElementById("txtPass").value;
  const diemToan = document.getElementById("txtDiemToan").value;
  const diemLy = document.getElementById("txtDiemLy").value;
  const diemHoa = document.getElementById("txtDiemHoa").value;

  return new SinhVien(msSV, ten, email, matKhau, diemToan, diemLy, diemHoa);
}

// xoá thông tin sau khi lấy từ form
function delet() {
  document.getElementById("txtMaSV").value = "";
  document.getElementById("txtTenSV").value = "";
  document.getElementById("txtEmail").value = "";
  document.getElementById("txtPass").value = "";
  document.getElementById("txtDiemToan").value = "";
  document.getElementById("txtDiemLy").value = "";
  document.getElementById("txtDiemHoa").value = "";
}

// hàm render thông tin sinh viên ra table
function renderDSSV(dssv) {
  var contentTable = "";
  for (var i = 0; i < dssv.length; i++) {
    var svIn4Oj = dssv[i];
    var svIn4 = `<tr>
        <td>${svIn4Oj.msSV}</td>
        <td>${svIn4Oj.ten}</td>
        <td>${svIn4Oj.email}</td>
        <td>${svIn4Oj.tinhDTB()}</td>
        <td>
        <button onclick="xoaSinhVien('${
          svIn4Oj.msSV
        }')" class = "btn btn-danger">Xoá</button>
        <button onclick = "suaSinhVien('${
          svIn4Oj.msSV
        }')" class = "btn btn-warning">Sửa</button>
        </td>
        </tr>`;
    contentTable += svIn4;
  }

  document.getElementById("tbodySinhVien").innerHTML = contentTable;
}

// hàm tìm kiêm vị trí sinh viên
function timSV(msSV, dssv) {
  for (var index = 0; index < dssv.length; index++) {
    var svIn4Oj = dssv[index];
    if (svIn4Oj.msSV == msSV) {
      return index;
    }
  }
  return -1;
}
// hàm tìm kiêm vị trí sinh viên
function timTenSV(tenSV, dssv) {
  console.log("tenSV: ", tenSV);
  for (var index = 0; index < dssv.length; index++) {
    var svIn4Oj = dssv[index];
    // console.log("svIn4Oj.ten: ", svIn4Oj.ten);
    if (svIn4Oj.ten == tenSV) {
      return index;
    }
  }
  return -1;
}

// hàm show thông tin để edit
function showThongTinlenForm(svIn4) {
  document.getElementById("txtMaSV").value = svIn4.msSV;
  document.getElementById("txtTenSV").value = svIn4.ten;
  document.getElementById("txtEmail").value = svIn4.email;
  document.getElementById("txtPass").value = svIn4.matKhau;
  document.getElementById("txtDiemToan").value = svIn4.diemToan;
  document.getElementById("txtDiemLy").value = svIn4.diemLy;
  document.getElementById("txtDiemHoa").value = svIn4.diemHoa;
}

// hàm tìm kiếm sinh vien băng tên ở chỗ search
document.getElementById("btnSearch").onclick = function () {
  var tenSv = document.getElementById("txtSearch").value;
};
